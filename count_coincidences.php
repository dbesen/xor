<?
	$file = file_get_contents("ciphertext.txt");
	$max = 0;
	for($i=1;$i<21;$i++) {
		$check = substr($file, $i) . substr($file, 0, $i);
		$count = 0;
		for($j=0;$j<strlen($check);$j++) {
			if($check{$j} == $file{$j}) $count++;
		}
		print("$i: $count: " . (100 * ($count / strlen($file))) . "\n");
		if($count > $max) {
			$keylen = $i;
			$max = $count;
		}
/*		if(($count / strlen($file)) > .03) {
			$keylen = $i;
			break;
		}
*/
	}


	print "Guess: $keylen chars in key\n";

//	$xor = substr($file, $keylen) . substr($file, 0, $keylen);
//	$xor ^= $file;

	$en_freq = get_en_freq();

	$chars = array();
	for($i=0;$i<strlen($file);$i++) {
		$chars[$i % $keylen][] = ord($file{$i});
	}

	$key = array();
	for($i=0;$i<$keylen;$i++) {
		$min_dist = -1;
		for($j=0;$j<256;$j++) {
			$freq = freq($chars[$i], $j);
			$dist = freq_dist($freq, $en_freq);
			if($min_dist == -1 || $dist < $min_dist) {
				$key[$i] = chr($j);
				$min_dist = $dist;
			}
		}
	}

	// todo: remove redundancy from key

	$key = join('', $key);

	print "Key: $key\n";

	$key = str_repeat($key, 1 + (strlen($file) / strlen($key)));

	file_put_contents("guess.txt", $file ^ $key);

	function freq($chars, $xor = 0) {
		$freq = array();
		if(!is_array($chars)) {
			die("chars is $chars");
		}
		foreach($chars as $char) {
			$freq[$char ^ $xor]++;
		}
		arsort($freq);
		return array_keys($freq);
	}

	function freq_dist($a, $b) {
		$b = array_flip($b);
		$count = 0;
		foreach($a as $rank=>$ch) {
			$count += abs($rank - $b[$ch]);
		}
		return $count;
	}

	function get_en_freq() {
		$file = file_get_contents("plaintext.txt");
		$chars = array();
		for($i=0;$i<strlen($file);$i++) {
			$chars[] = ord($file{$i});
		}
		return freq($chars);
	}


?>
